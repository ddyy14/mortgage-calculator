package edu.sjsu.android.mortgage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView result;
    private TextView annual_interest;
    private SeekBar setSeekBar;
    private RadioGroup radioGroup;
    private RadioButton button1, button2, button3;
    private CheckBox checkBox;
    private Button cal_button;
    private Button reset_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText)findViewById(R.id.editText);

        annual_interest = (TextView)findViewById(R.id.interestRate);
        annual_interest.setText("10");   // default value 10

        setSeekBar = (SeekBar)findViewById(R.id.seekBar);

        // create seekbar listener
        setSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // define interest value as double
                double interest = (Double)(progress / 1.0);
                annual_interest.setText(String.valueOf(interest));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        button1 = (RadioButton)findViewById(R.id.radioButton);
        button2 = (RadioButton)findViewById(R.id.radioButton2);
        button3 = (RadioButton)findViewById(R.id.radioButton3);
        checkBox = (CheckBox)findViewById(R.id.checkBox);
        result = (TextView)findViewById(R.id.finalResult);
        cal_button = (Button)findViewById(R.id.cal_button);


        // calculate pay back money per month
        cal_button.setOnClickListener(new OnClickListener(){
                        @Override
                            public void onClick(View view){
                                double monthlyPayment;
                                double mp = Double.valueOf(annual_interest.getText().toString());
                                mp /= 1200;

                                int loanOfYear = 15 * 12;

                            // if user click calculate without input nothing, edit text would show 0.0 in edit text instead of crash
                            if(editText.getText().length() == 0){
                                editText.setText("0.0");
                                editText.setSelection(editText.getText().toString().length());
                            }

                            // store the value that convert from edit text
                            double value = Double.valueOf(editText.getText().toString());
                            // check radio button
                            if(button1.isChecked()){
                               loanOfYear = 15 * 12;
                            }
                            if(button2.isChecked()){
                                loanOfYear = 20 * 12;
                            }
                            if(button3.isChecked()){
                                loanOfYear = 30 * 12;
                            }
                            // if interest is 0
                            if(mp == 0){
                                monthlyPayment = value / loanOfYear;
                            }else{
                                monthlyPayment = (double)(value * (mp / (1 - Math.pow(1 + mp, - loanOfYear))));
                            }
                            // checking the check box
                            if(checkBox.isChecked()){
                                monthlyPayment = monthlyPayment + 0.001 * value;
                            }
                            monthlyPayment = Math.round(monthlyPayment * 100) / 100.0;
                            result.setText("" + monthlyPayment);
                        }
        });
        // refresh button for user
        reset_button = (Button)findViewById(R.id.resetButton);
        reset_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                annual_interest.setText("10.0");
                setSeekBar.setProgress(setSeekBar.getMax() / 2);
                checkBox.setChecked(false);
                radioGroup.clearCheck();
                button1.setChecked(true);
                result.setText("0");
            }
        });
    }
}